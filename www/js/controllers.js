//maps api key AIzaSyDFDLpDE0bpov0hoP5M41zzBTGiCiItJ5A

angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    // console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('HomeCtrl', ['$scope','DataService','GeoCodingService',
'$ionicPopover','$ionicLoading','$state',
function($scope,DataService,GeoCodingService,
  $ionicPopover,$ionicLoading,$state) {



  $ionicLoading.show({
    template: 'Loading...',
    //duration: 3000
  });



  $scope.readyForMap = true;



  DataService.query().$promise.then(function (response) {

    var cntr = 0;
    $scope.dataset = [];



    response.map(function(item, index) {

       GeoCodingService.query({
          address: item.country
        }).$promise.then(function (result) {
          item.latitude = result.results[0].geometry.location.lat;
          item.longitude = result.results[0].geometry.location.lng;
          $scope.dataset.push(item);
          cntr++;

          if(cntr == response.length) {

            $scope.max = Math.max.apply(Math,$scope.dataset.map(function(o){return o.count;}));
            $scope.min = Math.min.apply(Math,$scope.dataset.map(function(o){return o.count;}));



            //console.log($scope.dataset);

            var getGeoJSON = function() {
              return new Promise(function(resolve,reject){
                resolve(GeoJSON.parse($scope.dataset, {Point: ['latitude', 'longitude']}));
              }) ;
            };

            getGeoJSON().then(function(result) {
              var map = new google.maps.Map(document.getElementById('map_div'), {
                  zoom: 2,
                  center: {
                    "lat" : 37.09024,
                    "lng" : -95.712891
                  },
                  mapTypeId: 'terrain',fullscreenControl: false
                });




              map.data.addGeoJson(result);

              map.data.addListener('click', function(event) {
                $ionicPopover.fromTemplateUrl('home-popover.html', {
                  scope: $scope
                }).then(function(popover) {

                  $scope.pointData = event.feature.f;
                  // console.log($scope.pointData);

                  for (var i in event) {
                    if(event[i] instanceof MouseEvent) {
                      var clickevent =  event[i];
                    }
                    
                  }

                  popover.show(clickevent);

                  $scope.closePopover = function() {
                     popover.hide();
                  };
                });

              });

              map.data.setStyle(function(feature) {
                var count = feature.getProperty('count');
                var scale = Math.log(count) * 5;


                // console.log(scale);

                $ionicLoading.hide();
                // $state.reload();
                return {
                  icon: getCircle(scale)
                };
              });


              function getCircle(scale) {
                  return {
                    path: google.maps.SymbolPath.CIRCLE,
                    fillColor: 'red',
                    fillOpacity: .4,
                    scale: scale,
                    strokeColor: 'white',
                    strokeWeight: .5
                  };
              }






            });




          }


        });


    });







    }, function (error) {
      //console.log(error);

    });

}])

.controller('CountryCtrl', [
  '$scope','CountryDataService','CountryCodeService',
'GeoCodingService','$ionicPopover', '$ionicLoading', '$state',
function($scope, CountryDataService, CountryCodeService, GeoCodingService,
  $ionicPopover, $ionicLoading, $state) {

  $ionicLoading.show({
    template: 'Loading...',
    //duration: 3000
  });


  $scope.readyForMap = true;



  CountryCodeService.query().$promise.then(function(country) {

  $scope.country = country[0][$state.params.id.toUpperCase()];

  var cid = ($state.params.id == 'us') ? $state.params.id : $state.params.id + 's';




  if($state.params.id == 'us') {
    CountryDataService(cid).query().$promise.then(function (result) {
     $scope.dataset = result;


     var getGeoJSON = function() {
       return new Promise(function(resolve,reject){
         resolve(GeoJSON.parse($scope.dataset, {Point: ['latitude', 'longitude']}));
       }) ;
     };

     getGeoJSON().then(function(result) {

       var map = new google.maps.Map(document.getElementById('map_country'), {
           zoom: 2,
           center: {
             "lat" : 37.09024,
             "lng" : -95.712891
           },
           mapTypeId: 'terrain',fullscreenControl: false
         });



         map.data.addGeoJson(result);

         $ionicLoading.hide();
         // $state.reload();

         map.data.addListener('click', function(event) {
           $ionicPopover.fromTemplateUrl('us-popover.html', {
             scope: $scope
           }).then(function(popover) {

             $scope.pointData = event.feature.f;
            //  console.log($scope.pointData);

            for (var i in event) {
              if(event[i] instanceof MouseEvent) {
                var clickevent =  event[i];
              }
              
            }

             popover.show(clickevent);

             $scope.closePopover = function() {
                popover.hide();
             };
           });

         });

         map.data.setStyle(function(feature) {

           var count = feature.getProperty('count');
           var scale = Math.log(count) * 2;


           return {
             icon: {
               path: google.maps.SymbolPath.CIRCLE,
               fillColor: 'red',
               fillOpacity: .2,
               scale: scale,
               strokeColor: 'white',
               strokeWeight: .5
             }
           };
         });



      });





     });
  }
  else {


    GeoCodingService.query({
        address: $scope.country
      }).$promise.then(function (response) {
      var centerPoint = response.results[0].geometry.location;
      CountryDataService(cid).query().$promise.then(function (response) {



        $scope.dataset = response;

        var getGeoJSON = function() {
          return new Promise(function(resolve,reject){
            resolve(GeoJSON.parse($scope.dataset, {Point: ['latitude', 'longitude']}));
          }) ;
        };

        getGeoJSON().then(function(result) {


          var centerPoint = {
            "lat": parseInt($scope.dataset[0]['latitude']),
            "lng": parseInt($scope.dataset[0]['longitude'])
          };
          // console.log(centerPoint);
          // $state.reload();


          var map = new google.maps.Map(document.getElementById('map_country'), {
              zoom: 2,
              center: centerPoint,
              mapTypeId: 'terrain',fullscreenControl: false
            });



          map.data.addGeoJson(result);

          $ionicLoading.hide();
          // $state.reload();

          map.data.addListener('click', function(event) {
            $scope.popover = $ionicPopover.fromTemplateUrl('point-popover.html', {
              scope: $scope
            }).then(function(popover) {
              $scope.pointData = event.feature.f;
              //console.log(event);
              for (var i in event) {
                if(event[i] instanceof MouseEvent) {
                  var clickevent =  event[i];
                }
                
              }
              popover.show(clickevent);
            });

          });

          map.data.setStyle(function(feature) {



            return {
              icon: {
                path: google.maps.SymbolPath.CIRCLE,
                fillColor: 'red',
                fillOpacity: .2,
                scale: 10,
                strokeColor: 'white',
                strokeWeight: .5
              }
            };
          });




        });


      });

    });
  }


  });











}])
.controller('USCtrl', [
  '$scope','USDataService','CountryCodeService',
'GeoCodingService','$ionicPopover', '$ionicLoading', '$state',
function($scope, USDataService, CountryCodeService, GeoCodingService,
  $ionicPopover, $ionicLoading, $state) {

  $ionicLoading.show({
    template: 'Loading...',
    //duration: 3000
  });


  $scope.readyForMap = true;

  USDataService($state.params.id).query().$promise.then(function (response) {

    $scope.state = $state.params.id;
    $scope.dataset = response;

    GeoCodingService.query({
        address: $scope.state + ' United States'
      }).$promise.then(function (res) {
      var centerPoint = res.results[0].geometry.location;




        var getGeoJSON = function() {
          return new Promise(function(resolve,reject){
            resolve(GeoJSON.parse($scope.dataset, {Point: ['latitude', 'longitude']}));
          }) ;
        };

        getGeoJSON().then(function(result) {

          // $state.reload();


          var map = new google.maps.Map(document.getElementById('map_us'), {
              zoom: 2,
              center: centerPoint,
              mapTypeId: 'terrain',fullscreenControl: false
            });



          map.data.addGeoJson(result);

          $ionicLoading.hide();
          // $state.reload();

          map.data.addListener('click', function(event) {
            $scope.popover = $ionicPopover.fromTemplateUrl('point-popover.html', {
              scope: $scope
            }).then(function(popover) {
              $scope.pointData = event.feature.f;
              //console.log(event);
              for (var i in event) {
                if(event[i] instanceof MouseEvent) {
                  var clickevent =  event[i];
                }
                
              }
              popover.show(clickevent);
            });

          });

          map.data.setStyle(function(feature) {



            return {
              icon: {
                path: google.maps.SymbolPath.CIRCLE,
                fillColor: 'red',
                fillOpacity: .2,
                scale: 10,
                strokeColor: 'white',
                strokeWeight: .5
              }
            };
          });




        });




    });



  });











}]);
