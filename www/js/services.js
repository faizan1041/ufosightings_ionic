angular.module('starter.services', ['ngResource'])
// .constant("baseURL","http://localhost:3000/api/")
.constant("baseURL","http://ec2-13-126-16-242.ap-south-1.compute.amazonaws.com:3000/api/")
.service('DataService', ['$resource', 'baseURL', function($resource,baseURL) {

  var accesstoken = "2UhG4HjsF6g6MGe4Gsbm9t36JVGav10CacOZtlhmDDLBjHa21EKSHnL9qzGLvFJf";
  return $resource(baseURL+"DFNamedLocations",{}, {
          query: {
            method: 'GET',
            headers: {
                'Accept':'application/json',
                'Content-Type': 'application/json',
                'X-Access-Token': accesstoken
            },
            isArray:true
          }
        })

}])

.service('CountryDataService', ['$resource','baseURL','$stateParams',
 function($resource,baseURL,$stateParams) {




  var accesstoken = "2UhG4HjsF6g6MGe4Gsbm9t36JVGav10CacOZtlhmDDLBjHa21EKSHnL9qzGLvFJf";

  return function(cid) {
       return $resource(baseURL+cid,{}, {
              query: {
                method: 'GET',
                headers: {
                    'Accept':'application/json',
                    'Content-Type': 'application/json',
                    'X-Access-Token': accesstoken
                },
                isArray:true
              }
            });
  };



}])
.service('USDataService', ['$resource','baseURL','$stateParams',
 function($resource,baseURL,$stateParams) {



  var accesstoken = "2UhG4HjsF6g6MGe4Gsbm9t36JVGav10CacOZtlhmDDLBjHa21EKSHnL9qzGLvFJf";

  return function(sid) {
    return $resource(baseURL+'us/'+sid ,{}, {
            query: {
              method: 'GET',
              headers: {
                  'Accept':'application/json',
                  'Content-Type': 'application/json',
                  'X-Access-Token': accesstoken
              },
              isArray:true
            }
          });
  };



}])
.service('CountryCodeService', ['$resource','baseURL','$stateParams',
 function($resource,baseURL,$stateParams) {


  var cid = ($stateParams.id == 'us') ? $stateParams.id : $stateParams.id + 's';

  var accesstoken = "2UhG4HjsF6g6MGe4Gsbm9t36JVGav10CacOZtlhmDDLBjHa21EKSHnL9qzGLvFJf";
  return $resource(baseURL+'Countries',{}, {
          query: {
            method: 'GET',
            headers: {
                'Accept':'application/json',
                'Content-Type': 'application/json',
                'X-Access-Token': accesstoken
            },
            isArray:true
          }
        })

}])
.service('GeoCodingService', ['$resource', 'baseURL', function($resource,baseURL) {

  var accesstoken = "2UhG4HjsF6g6MGe4Gsbm9t36JVGav10CacOZtlhmDDLBjHa21EKSHnL9qzGLvFJf";
  return $resource("http://maps.google.com/maps/api/geocode/json",{}, {
          query: {
            method: 'GET',
            // params: {
            //   address:
            // },
            // headers: {
            //     'Accept':'application/json',
            //     'Content-Type': 'application/json',
            //     'X-Access-Token': accesstoken
            // },
            //isArray:true
          }
        })

}]);
